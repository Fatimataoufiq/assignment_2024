package ma.nice.store.Controller;

import ma.nice.store.entity.Product;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.dto.request.SupplyRequest;
import ma.nice.store.mapper.ProductMapper;
import ma.nice.store.mapper.SupplyMapper;
import ma.nice.store.service.SupplyService;
import ma.nice.store.web.ProductExpiryAlertsController;
import ma.nice.store.web.common.PathEndpoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductExpiryAlertsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private SupplyService supplyService;

    @Autowired
    private SupplyMapper supplyMapper;
    @Autowired
    private ProductMapper productMapper;

    @InjectMocks
    private ProductExpiryAlertsController productExpiryAlertsController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(productExpiryAlertsController).build();
    }

    @Test
    public void testListProductsNearExpiryDate() throws Exception {
        int thresholdDays = 14;
        List<ProductRequest> products = new ArrayList<>();
        SupplyRequest supplyDTO_1 = new SupplyRequest();
        supplyDTO_1.setProductCode("TEST1");
        supplyDTO_1.setQuantity(5);
        supplyDTO_1.setProductExpirationDate(LocalDateTime.parse("2023-11-30T14:30:00"));
       Product product_1= supplyMapper.toEntity(supplyDTO_1).getProduct();
        SupplyRequest supplyDTO_2 = new SupplyRequest();
        supplyDTO_2.setProductCode("TEST2");
        supplyDTO_2.setQuantity(5);
        supplyDTO_2.setProductExpirationDate(LocalDateTime.parse("2023-11-20T14:30:00"));
        Product product_2= supplyMapper.toEntity(supplyDTO_2).getProduct();
        products.add(productMapper.toDTO(product_1));
        products.add(productMapper.toDTO(product_2));


        // Adjust the return type to match the actual return type of supplyService.getProductsNearExpiryDate
        Mockito.when(supplyService.getProductsNearExpiryDate(thresholdDays)).thenReturn(products);

        mockMvc.perform(MockMvcRequestBuilders.get("/" + PathEndpoint.EXPIRY_ALERTS)
                        .param("thresholdDays", String.valueOf(thresholdDays)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
