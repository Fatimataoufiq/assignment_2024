package ma.nice.store.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nice.store.dto.request.SupplyRequest;
import ma.nice.store.service.SupplyService;
import ma.nice.store.web.SupplyController;
import ma.nice.store.web.common.PathEndpoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.beans.factory.annotation.Autowired; // Ajoutez cette importation

import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
public class SupplyControllerTest {
    private MockMvc mockMvc;
    @Mock
    private SupplyService supplyService;
    @InjectMocks
    private SupplyController supplyController;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(supplyController).build();
    }

    @Test
    public void testAddSupply() throws Exception {
        SupplyRequest supplyRequest = new SupplyRequest();
        supplyRequest.setProductCode("TEST1");
        supplyRequest.setQuantity(5);
        supplyRequest.setProductExpirationDate(LocalDateTime.parse("2023-11-30T14:30:00"));

        Mockito.when(supplyService.addProductToInventory(supplyRequest)).thenReturn(1L);

        String supplyJson = objectMapper.writeValueAsString(supplyRequest);

        mockMvc.perform(MockMvcRequestBuilders.post("/" + PathEndpoint.SUPPLY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(supplyJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }
}
