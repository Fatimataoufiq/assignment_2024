package ma.nice.store.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nice.store.dto.request.ReturnRequest;
import ma.nice.store.service.ReturnProductService;
import ma.nice.store.web.ReturnProductController;
import ma.nice.store.web.common.PathEndpoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class ReturnProductControllerTest {
    private MockMvc mockMvc;

    @Mock
    private ReturnProductService returnProductService;

    @InjectMocks
    private ReturnProductController returnProductController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(returnProductController).build();
    }

    @Test
    public void testAddReturnProduct() throws Exception {
        ReturnRequest returnRequest = new ReturnRequest();
        returnRequest.setProductCode("TEST1");
        returnRequest.setQuantity(5);
        returnRequest.setReason("Reason");

        Mockito.when(returnProductService.returnProduct(returnRequest)).thenReturn(1L);

        ObjectMapper objectMapper = new ObjectMapper();
        String returnProductJson = objectMapper.writeValueAsString(returnRequest);

        mockMvc.perform(MockMvcRequestBuilders.post("/"+ PathEndpoint.RETURN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(returnProductJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }
}
