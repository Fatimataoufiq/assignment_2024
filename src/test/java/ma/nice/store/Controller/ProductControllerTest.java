package ma.nice.store.Controller;


import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.service.ProductService;
import ma.nice.store.web.ProductController;
import ma.nice.store.web.common.ExceptionHandlingController;
import ma.nice.store.web.common.PathEndpoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;



@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(productController)
                .setControllerAdvice(new ExceptionHandlingController())
                .build();
    }

    @Test
    public void testCreateProduct() throws Exception {
        ProductRequest productRequest = new ProductRequest();
        productRequest.setProductName("Product 1");
        productRequest.setProductCode("TEST1");
        productRequest.setDescription("Test description");
        productRequest.setUnitPrice(16.0);
        productRequest.setQuantityInStock(100);
        productRequest.setQuantityThreshold(100);

        when(productService.addProduct(any(ProductRequest.class))).thenReturn(1L);

        String json = "{\"productName\":\"Product 1\",\"productCode\":\"TEST1\",\"description\":\"Test description\",\"unitPrice\":16.0,\"quantityInStock\":100,\"quantityThreshold\":100}";

        mockMvc.perform(MockMvcRequestBuilders.post("/"+ PathEndpoint.PRODUCT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }




    @Test
    public void testGetAllProducts() throws Exception {
        List<ProductRequest> products = new ArrayList<>();
        ProductRequest product1 = new ProductRequest();
        product1.setProductName("Product 1");
        product1.setProductCode("TEST1");
        product1.setDescription("Test description");
        product1.setUnitPrice(16.0);
        product1.setQuantityInStock(100);
        product1.setQuantityThreshold(100);

        ProductRequest product2 = new ProductRequest();
        product2.setProductName("Product 2");
        product2.setProductCode("TEST2");
        product2.setDescription("Test description for product 2");
        product2.setUnitPrice(16.0);
        product2.setQuantityInStock(200);
        product2.setQuantityThreshold(150);

        products.add(product1);
        products.add(product2);

        when(productService.listProducts()).thenReturn(products);

        mockMvc.perform(get("/"+ PathEndpoint.PRODUCT))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testGetProductById() throws Exception {
        Long productId = 1L;
        ProductRequest product = new ProductRequest();
        product.setProductName("Product 1");
        product.setProductCode("TEST1");
        product.setDescription("Test description");
        product.setUnitPrice(16.0);
        product.setQuantityInStock(100);
        product.setQuantityThreshold(100);

        when(productService.getProductById(productId)).thenReturn(product);

        mockMvc.perform(get("/"+ PathEndpoint.PRODUCT + "/" + productId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testDeleteProduct() throws Exception {
        Long productId = 1L;

        when(productService.deleteProduct(productId)).thenReturn(true);

        mockMvc.perform(delete("/" + PathEndpoint.PRODUCT + "/" + productId))
                .andExpect(status().isOk());
    }
}
