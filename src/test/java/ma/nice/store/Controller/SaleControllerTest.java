package ma.nice.store.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nice.store.dto.request.SaleRequest;
import ma.nice.store.service.SaleService;
import ma.nice.store.web.SaleController;
import ma.nice.store.web.common.PathEndpoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class SaleControllerTest {

    private MockMvc mockMvc;

    @Mock
    private SaleService saleService;

    @InjectMocks
    private SaleController saleController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(saleController).build();
    }
    @Test
    public void testAddSale() throws Exception {
        SaleRequest saleDTO = new SaleRequest();
        saleDTO.setProductCode("TEST1");
        saleDTO.setQuantity(5);

        when(saleService.addSale(saleDTO)).thenReturn(1L);

        ObjectMapper objectMapper = new ObjectMapper();
        String saleJson = objectMapper.writeValueAsString(saleDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/"+ PathEndpoint.SALE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(saleJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }
}
