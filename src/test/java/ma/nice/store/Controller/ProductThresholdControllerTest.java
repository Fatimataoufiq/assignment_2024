package ma.nice.store.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.dto.request.SetThresholdRequest;
import ma.nice.store.service.ProductService;
import ma.nice.store.web.ProductThresholdController;
import ma.nice.store.web.common.PathEndpoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductThresholdControllerTest {
    private MockMvc mockMvc;

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductThresholdController productThresholdController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(productThresholdController).build();
    }

    @Test
    public void shouldSetThreshold() throws Exception {
        SetThresholdRequest setThresholdCommand = new SetThresholdRequest();
        setThresholdCommand.setQuantityThreshold(10);
        setThresholdCommand.setProductCode("A123");
        ObjectMapper objectMapper = new ObjectMapper();
        String setThresholdJson = objectMapper.writeValueAsString(setThresholdCommand);

        mockMvc.perform(MockMvcRequestBuilders.post("/set-threshold")
                        .contentType("application/json")
                        .content(setThresholdJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        verify(productService, times(1)).setThreshold(setThresholdCommand);
    }

    @Test
    public void testGetAllProductsBelowThreshold() throws Exception {
        List<ProductRequest> productRequests = Collections.singletonList(new ProductRequest());

        when(productService.getAllProductsBelowThreshold()).thenReturn(productRequests);

        mockMvc.perform(MockMvcRequestBuilders.get("/" + PathEndpoint.THRESHOLD_ALERTS)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
