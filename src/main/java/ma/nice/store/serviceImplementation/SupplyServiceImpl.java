package ma.nice.store.serviceImplementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nice.store.entity.Product;
import ma.nice.store.entity.Supply;
import ma.nice.store.entity.util.EventType;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.dto.request.SupplyRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.mapper.ProductMapper;
import ma.nice.store.mapper.SupplyMapper;
import ma.nice.store.repository.SupplyRepository;
import ma.nice.store.service.ProductService;
import ma.nice.store.service.SupplyService;
import ma.nice.store.service.TransactionHistoryService;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SupplyServiceImpl implements SupplyService {
    private final SupplyRepository supplyRepository;
    private final ProductService productService;
    private final TransactionHistoryService transactionHistoryService;
    private final ProductMapper productMapper;
    private final SupplyMapper supplyMapper;


    @Transactional
    @Override
    public Long addProductToInventory(SupplyRequest supplyRequest) throws ProductNotFound {
        Product product = productService.getProductByCode(supplyRequest.getProductCode());

        if (product == null) {
            throw new ProductNotFound("Product with code : " +  supplyRequest.getProductCode()+ " not found " );
        }
        Supply supply= new Supply();
        supply.setProduct(product);
        supply.setQuantity(supplyRequest.getQuantity());
        supply.setProductExpirationDate(supplyRequest.getProductExpirationDate());

       supplyRepository.save(supply);

        Integer newQuantity = supplyRequest.getQuantity() + product.getQuantityInStock();

        product.setQuantityInStock(newQuantity);

        transactionHistoryService.createTransaction(product,EventType.SUPPLY, supplyRequest.getQuantity());

        return supply.getSupplyId();
    }

    @Override
    public List<ProductRequest> getProductsNearExpiryDate(int thresholdDays) {
        LocalDate currentDate = LocalDate.now();
        LocalDateTime thresholdStartDate = LocalDate.now().atStartOfDay();
        LocalDateTime thresholdEndDate = currentDate.plusDays(thresholdDays).atTime(23, 59, 59);

        List<Supply> supplies = supplyRepository.findByProductExpirationDateBetween(thresholdStartDate, thresholdEndDate);

        return supplies.stream()
                .map(supply -> productMapper.toDTO(supply.getProduct()))
                .collect(Collectors.toList());
    }



}
