package ma.nice.store.serviceImplementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nice.store.entity.Product;
import ma.nice.store.entity.Sale;
import ma.nice.store.entity.util.EventType;
import ma.nice.store.dto.request.SaleRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.exceptions.ProductQuantityNotInStock;
import ma.nice.store.mapper.SaleMapper;
import ma.nice.store.repository.SaleRepository;
import ma.nice.store.service.ProductService;
import ma.nice.store.service.SaleService;
import ma.nice.store.service.TransactionHistoryService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SaleServiceImpl implements SaleService {
    private final ProductService productService;
    private final SaleRepository saleRepository;
    private final TransactionHistoryService transactionHistoryService;
    private final SaleMapper saleMapper;

    @Transactional
    @Override
    public Long addSale(SaleRequest saleRequest) throws ProductNotFound, ProductQuantityNotInStock {
        Product product = productService.getProductByCode(saleRequest.getProductCode());
        if (product.getQuantityInStock() < saleRequest.getQuantity()){
            throw new ProductQuantityNotInStock("The requested quantity is not available in the stock");
        }
        Sale sale= new Sale();
        sale.setProduct(product);
        sale.setUser(null);
        sale.setSoldQuantity(saleRequest.getQuantity());
        sale.setTotalPrice(product.getUnitPrice() * saleRequest.getQuantity());
        saleRepository.save(sale);

        product.setQuantityInStock(product.getQuantityInStock() - saleRequest.getQuantity());

        transactionHistoryService.createTransaction(product, EventType.SALE, saleRequest.getQuantity());

        return sale.getSaleId();
    }
}
