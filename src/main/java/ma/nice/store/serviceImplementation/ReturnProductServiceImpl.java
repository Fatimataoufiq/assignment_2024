package ma.nice.store.serviceImplementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nice.store.entity.Product;
import ma.nice.store.entity.ReturnProduct;
import ma.nice.store.entity.util.EventType;
import ma.nice.store.dto.request.ReturnRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.mapper.ReturnProductMapper;
import ma.nice.store.repository.ReturnProductRepository;
import ma.nice.store.service.ProductService;
import ma.nice.store.service.ReturnProductService;
import ma.nice.store.service.TransactionHistoryService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReturnProductServiceImpl implements ReturnProductService {
    private final ReturnProductRepository returnProductRepository;
    private final ProductService productService;
    private final TransactionHistoryService transactionHistoryService;
    private final ReturnProductMapper returnProductMapper;

    @Transactional
    @Override
    public Long returnProduct(ReturnRequest returnRequest) throws ProductNotFound {
        Product product = productService.getProductByCode(returnRequest.getProductCode());
        if (product == null) {
            throw new ProductNotFound("Product with code : " +  returnRequest.getProductCode()+ " not found " );
        }
        ReturnProduct returnProduct = returnProductMapper.toEntity(returnRequest);

        returnProductRepository.save(returnProduct);

        Integer newQuantity = returnRequest.getQuantity() + product.getQuantityInStock();
        product.setQuantityInStock(newQuantity);

        transactionHistoryService.createTransaction(product, EventType.RETURN, returnRequest.getQuantity());

        return returnProduct.getReturnId();
    }
}
