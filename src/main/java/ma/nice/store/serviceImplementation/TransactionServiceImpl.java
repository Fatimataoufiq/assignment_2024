package ma.nice.store.serviceImplementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nice.store.entity.Product;
import ma.nice.store.entity.TransactionHistory;
import ma.nice.store.entity.util.EventType;
import ma.nice.store.mapper.TransactionMapper;
import ma.nice.store.repository.TransactionHistoryRepository;
import ma.nice.store.service.TransactionHistoryService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionHistoryService {
    private final TransactionMapper transactionMapper;
    private final TransactionHistoryRepository transactionHistoryRepository;


    @Transactional
    @Override
    public Long createTransaction(Product product, EventType eventType, Integer quantity) {
        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setTransactionType(eventType);
        transactionHistory.setProduct(product);
        transactionHistory.setQuantity(quantity);
        transactionHistoryRepository.save(transactionHistory);

        return transactionHistory.getTransactionId();
    }
}
