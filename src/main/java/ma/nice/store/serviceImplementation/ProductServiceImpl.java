package ma.nice.store.serviceImplementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nice.store.entity.Product;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.dto.request.SetThresholdRequest;
import ma.nice.store.exceptions.ProductAlreadyExists;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.mapper.ProductMapper;
import ma.nice.store.repository.ProductRepository;
import ma.nice.store.service.ProductService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {


  private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Transactional
    @Override
    public Long addProduct(ProductRequest productRequest) throws ProductAlreadyExists {
        Optional<Product> existingProduct = productRepository.findByProductCode(productRequest.getProductCode());

        if (existingProduct.isPresent()) {
            throw new ProductAlreadyExists("Product with code: " + productRequest.getProductCode() + " already exists");
        }

        Product newProduct = productMapper.toEntity(productRequest);

       productRepository.save(newProduct);
       return newProduct.getProductId();

    }

    @Override
    public List<ProductRequest> listProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream()
                .map(productMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ProductRequest getProductById(Long id) throws ProductNotFound {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFound("Product with id: " + id + " not found"));

        return productMapper.toDTO(product);
    }


    @Override
    public Product getProductByCode(String productCode) throws ProductNotFound {
         return productRepository.findByProductCode(productCode)
                 .orElseThrow(()-> new ProductNotFound("Product with id: {} "+ productCode + " not found"));
    }

    @Transactional
    @Override
    public boolean deleteProduct(Long id) throws ProductNotFound {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFound("Product with id: " + id + " not found"));

        productRepository.delete(product);
        return true;
    }

    //Function to defind the quantity threshold for a product
    @Transactional
    public void setThreshold(SetThresholdRequest thresholdRequest) throws ProductNotFound {
        Product product = productRepository.findByProductCode(thresholdRequest.getProductCode())
                .orElseThrow(() -> new ProductNotFound("Product non found : " + thresholdRequest.getProductCode()));

        product.setQuantityThreshold(thresholdRequest.getQuantityThreshold());
        productRepository.save(product);
    }

    @Override
    public List<ProductRequest> getAllProductsBelowThreshold() {
        List<Product> products = productRepository.findProductsBelowThreshold();
        if(products.isEmpty()){
            return null;
        }
        else {
            return products.stream()
                    .map(productMapper::toDTO)
                    .collect(Collectors.toList());
        }
    }

}
