package ma.nice.store.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Entity
@Table(name = "Returns")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReturnProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long returnId;

    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;

    private Integer returnQuantity;

    private String reason;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime returnDate;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

}
