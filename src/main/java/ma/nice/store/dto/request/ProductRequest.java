package ma.nice.store.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.nice.store.dto.validator.NotEmptyString;
import ma.nice.store.dto.validator.ValidateProductCode;
import ma.nice.store.dto.validator.ValidateQuantity;
import ma.nice.store.dto.validator.ValidateUnitPrice;


@AllArgsConstructor @NoArgsConstructor @Data @Builder
public class ProductRequest {
    private Long productId;
    @ValidateProductCode
    private String productCode;
    @NotEmptyString
    private String productName;

    @NotEmptyString
    private String description;
    @ValidateUnitPrice
    private Double unitPrice;

    @ValidateQuantity
    private Integer quantityInStock;

    @ValidateQuantity
    private Integer quantityThreshold;

}
