package ma.nice.store.dto.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.nice.store.dto.validator.NotEmptyString;
import ma.nice.store.dto.validator.ValidateQuantity;

import java.time.LocalDateTime;


@Data @Builder @AllArgsConstructor @NoArgsConstructor
public class SupplyRequest {
    @NotEmptyString
    private String productCode;

    @ValidateQuantity
    @Max(value = 500, message = "quantity should be less than 500")
    private Integer quantity;
    private LocalDateTime productExpirationDate;

}