package ma.nice.store.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.nice.store.dto.validator.NotEmptyString;
import ma.nice.store.dto.validator.ValidateQuantity;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class SaleRequest {
    @NotEmptyString
    private String productCode;
    @ValidateQuantity
    private Integer quantity;
}
