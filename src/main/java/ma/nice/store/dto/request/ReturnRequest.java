package ma.nice.store.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.nice.store.dto.validator.NotEmptyString;
import ma.nice.store.dto.validator.ValidateQuantity;

@Data @Builder @AllArgsConstructor @NoArgsConstructor
public class ReturnRequest {
    @NotEmptyString
    private String productCode;

    @ValidateQuantity
    private Integer quantity;

    @NotEmptyString
    private String reason;
}