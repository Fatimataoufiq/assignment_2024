package ma.nice.store.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.nice.store.entity.Product;
import ma.nice.store.entity.User;
import ma.nice.store.entity.util.EventType;

import java.time.LocalDateTime;

@Data @Builder @AllArgsConstructor @NoArgsConstructor
public class TransactionHistoryRequest {
    private Product product;
    private Integer quantity;
    private EventType transactionType;

    private LocalDateTime transactionDate;
    private User user;
}
