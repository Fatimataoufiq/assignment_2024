package ma.nice.store.dto.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotEmptyStringValidator.class)
public @interface NotEmptyString {
    String message() default "Field should not be null or empty";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default{};
}
