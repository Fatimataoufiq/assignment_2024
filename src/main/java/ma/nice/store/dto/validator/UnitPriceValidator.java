package ma.nice.store.dto.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class UnitPriceValidator implements ConstraintValidator<ValidateUnitPrice, Double> {
    @Autowired
    private ValidationMessageBuilder validationMessageBuilder;

    @Override
    public void initialize(ValidateUnitPrice constraintAnnotation) {
    }

    @Override
    public boolean isValid(Double unitPrice, ConstraintValidatorContext context) {
        if (unitPrice == null) {
            validationMessageBuilder.addCustomErrorMessage(context, "Unit price should not be null");
            return false;
        }
        if (unitPrice < 0) {
            validationMessageBuilder.addCustomErrorMessage(context, "Unit price should not be negative");
            return false;
        }
        return true;
    }
}
