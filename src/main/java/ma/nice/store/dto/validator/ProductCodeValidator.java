package ma.nice.store.dto.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductCodeValidator implements ConstraintValidator<ValidateProductCode, String> {
    @Autowired
    private ValidationMessageBuilder validationMessageBuilder;

    @Override
    public void initialize(ValidateProductCode constraintAnnotation) {
    }

    @Override
    public boolean isValid(String productCode, ConstraintValidatorContext context) {
        boolean isValid = true;
        if (productCode == null) {
            validationMessageBuilder.addCustomErrorMessage(context, "Product code should not be null");
            //return false;
            isValid = false;
        }
        if (productCode.isEmpty()) {
            validationMessageBuilder.addCustomErrorMessage(context, "Product code should not be empty");
            isValid = false;
           // return false;
        }
        if (productCode.length() < 3) {
            validationMessageBuilder.addCustomErrorMessage(context, "Product code is too short");
            isValid = false;
           // return false;
        }
        if (productCode.length() > 10) {
            validationMessageBuilder.addCustomErrorMessage(context, "Product code is too long");
            isValid = false;
           // return false;
        }
        return isValid;
    }
}
