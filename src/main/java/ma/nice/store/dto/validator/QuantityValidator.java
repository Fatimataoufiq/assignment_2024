package ma.nice.store.dto.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class QuantityValidator implements ConstraintValidator<ValidateQuantity, Integer> {
    @Autowired
    private  ValidationMessageBuilder validationMessageBuilder;

    @Override
    public void initialize(ValidateQuantity constraintAnnotation) {
    }

    @Override
    public boolean isValid(Integer quantityInStock, ConstraintValidatorContext context) {
        if (quantityInStock == null) {
            validationMessageBuilder.addCustomErrorMessage(context, "Quantity should not be null");
            return false;
        }
        if (quantityInStock <= 0) {
            validationMessageBuilder.addCustomErrorMessage(context, "Quantity should not be less than or equal to 0");
            return false;
        }
        return true;
    }
}
