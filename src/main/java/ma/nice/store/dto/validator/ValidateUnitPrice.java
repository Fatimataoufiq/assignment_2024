package ma.nice.store.dto.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Constraint(validatedBy = UnitPriceValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateUnitPrice {
    String message() default "Unit price is invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
