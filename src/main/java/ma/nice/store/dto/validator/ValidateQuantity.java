package ma.nice.store.dto.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Constraint(validatedBy = QuantityValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateQuantity {
    String message() default "Product quantity is invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
