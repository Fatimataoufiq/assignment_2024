package ma.nice.store.dto.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class NotEmptyStringValidator implements ConstraintValidator<NotEmptyString, String> {
    @Override
    public void initialize(NotEmptyString constraintAnnotation){}

    @Override
    public boolean isValid(String value,  ConstraintValidatorContext context){
        return value != null && !value.isEmpty();
    }
}
