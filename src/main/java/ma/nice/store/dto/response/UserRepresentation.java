package ma.nice.store.dto.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRepresentation {
    private Long userId;

    private String username;

    private String role;

    private LocalDateTime creationDate;

    private LocalDateTime lastLoginDate;
}
