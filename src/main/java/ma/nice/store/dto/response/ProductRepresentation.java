package ma.nice.store.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductRepresentation {
    private Long productId;
    private String productCode;
    private String productName;
    private String description;
    private Double unitPrice;
    private Integer quantityInStock;
    private LocalDateTime creationDate;
    private LocalDateTime modificationDate;
    private Integer quantityThreshold;
}
