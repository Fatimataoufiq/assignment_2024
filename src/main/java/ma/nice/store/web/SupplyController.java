package ma.nice.store.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.nice.store.dto.request.SupplyRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.service.SupplyService;
import ma.nice.store.web.common.PathEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PathEndpoint.SUPPLY)
@RequiredArgsConstructor
@Slf4j
public class SupplyController {
    private final SupplyService supplyService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long addProductToInventory(@Valid @RequestBody SupplyRequest supplyRequest) throws ProductNotFound{
        log.info("Create a new Supply Transaction : {} ", supplyRequest);
        return  supplyService.addProductToInventory(supplyRequest);
    }
}
