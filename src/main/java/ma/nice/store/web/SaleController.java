package ma.nice.store.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.nice.store.dto.request.SaleRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.exceptions.ProductQuantityNotInStock;
import ma.nice.store.service.SaleService;
import ma.nice.store.web.common.PathEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PathEndpoint.SALE)
@RequiredArgsConstructor
@Slf4j
public class SaleController {

    private final SaleService saleService;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long addSale(@Valid @RequestBody SaleRequest saleRequest) throws ProductNotFound, ProductQuantityNotInStock{
        log.info("New Sale for product : {} ", saleRequest.getProductCode());
        return saleService.addSale(saleRequest);
    }

}
