package ma.nice.store.web;

import java.util.List;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.exceptions.ProductAlreadyExists;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.exceptions.ProductValidationException;
import ma.nice.store.service.ProductService;
import ma.nice.store.web.common.PathEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PathEndpoint.PRODUCT)
@RequiredArgsConstructor
@Slf4j
public class ProductController {

    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long addProduct(@Valid @RequestBody ProductRequest productRequest) throws ProductAlreadyExists, ProductValidationException {
        Long idProduct = productService.addProduct(productRequest);
        return idProduct;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductRequest> getAllProducts() {
        List<ProductRequest> products = productService.listProducts();
        log.info("All products existing in stock: {}", products);
        return products;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductRequest getProduct(@PathVariable Long id) throws ProductNotFound {
        log.info("Show product by Id : {}", id);
        ProductRequest productRequest = productService.getProductById(id);
        return productRequest;
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteProduct(@PathVariable Long id) throws ProductNotFound {
        return productService.deleteProduct(id);
    }
}
