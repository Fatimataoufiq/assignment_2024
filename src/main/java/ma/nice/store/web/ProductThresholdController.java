package ma.nice.store.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.dto.request.SetThresholdRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.service.ProductService;
import ma.nice.store.web.common.PathEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ProductThresholdController {
    private final ProductService productService;

    @PostMapping(PathEndpoint.SET_THRESHOLD)
    @ResponseStatus(HttpStatus.CREATED)
    public String setThreshold(@Valid @RequestBody SetThresholdRequest thresholdRequest) throws ProductNotFound {
        log.info("Setting the threshold for product: {}", thresholdRequest.getProductCode());
        productService.setThreshold(thresholdRequest);
        return "Threshold set successfully";
    }

    @GetMapping(PathEndpoint.THRESHOLD_ALERTS)
    @ResponseStatus(HttpStatus.OK)
    public List<ProductRequest> getAllProductsBelowThreshold() {
        log.info("Showing the list of products below threshold");
        return productService.getAllProductsBelowThreshold();
    }

}
