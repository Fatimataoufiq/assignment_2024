package ma.nice.store.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.nice.store.dto.request.ReturnRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.service.ReturnProductService;
import ma.nice.store.web.common.PathEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(PathEndpoint.RETURN)
@RequiredArgsConstructor
@Slf4j
public class ReturnProductController {
    private final ReturnProductService returnProductService;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long addReturnProduct(@Valid @RequestBody ReturnRequest returnRequest) throws ProductNotFound {
        log.info("Return Product to stock : {} ", returnRequest.getProductCode());
        return returnProductService.returnProduct(returnRequest);
    }
}
