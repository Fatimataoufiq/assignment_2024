package ma.nice.store.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.service.SupplyService;
import ma.nice.store.web.common.PathEndpoint;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PathEndpoint.EXPIRY_ALERTS)
@RequiredArgsConstructor
@Slf4j
public class ProductExpiryAlertsController {
    private final SupplyService supplyService;
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductRequest> listProductsNearExpiryDate(@RequestParam int thresholdDays) {
        log.info("Listing products nearing their date");
        List<ProductRequest> productsNearExpiry = supplyService.getProductsNearExpiryDate(thresholdDays);
        return productsNearExpiry;
    }

}
