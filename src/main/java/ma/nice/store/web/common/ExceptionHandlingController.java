package ma.nice.store.web.common;

import ma.nice.store.exceptions.ProductAlreadyExists;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.exceptions.ProductQuantityNotInStock;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(ProductNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Object handleProductNotFoundException(ProductNotFound ex) {
        Map<String, Object> response = new HashMap<>();
        response.put("message", "Product not found");
        response.put("HttpStatus", HttpStatus.NOT_FOUND);
        return response;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleProductValidationException(MethodArgumentNotValidException ex) {
        Map<String, Object> response = new HashMap<>();
        response.put("message", "Invalid product data");
        response.put("HttpStatus", HttpStatus.BAD_REQUEST);
        return response;
    }


    @ExceptionHandler(ProductAlreadyExists.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public Object handleProductAlreadyExistsException(ProductAlreadyExists ex) {
        Map<String, Object> response = new HashMap<>();
        response.put("message", "Product with the given code already exists");
        response.put("HttpStatus", HttpStatus.CONFLICT);
        return response;
    }

    @ExceptionHandler(ProductQuantityNotInStock.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleProductQuantityNotInStock(ProductQuantityNotInStock ex) {
        Map<String, Object> response = new HashMap<>();
        response.put("message", "The requested quantity is not available in the stock");
        response.put("HttpStatus", HttpStatus.BAD_REQUEST);
        return response;
    }



}
