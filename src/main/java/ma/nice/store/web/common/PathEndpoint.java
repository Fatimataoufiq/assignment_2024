package ma.nice.store.web.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PathEndpoint {
    public static final String AUTH = "auth";
    public static final String PRODUCT = "products";
    public static final String SUPPLY = "supply";
    public static final String SALE = "sale";
    public static final String RETURN = "return";
    public static final String EXPIRY_ALERTS ="expiry-alerts";
    public static final String THRESHOLD_ALERTS= "threshold-alerts";
    public static final String SET_THRESHOLD = "set-threshold";

}
