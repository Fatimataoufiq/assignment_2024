package ma.nice.store.service;

import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.dto.request.SupplyRequest;
import ma.nice.store.exceptions.ProductNotFound;

import java.util.List;

public interface SupplyService {
    Long addProductToInventory(SupplyRequest supplyRequest) throws ProductNotFound;

    List<ProductRequest> getProductsNearExpiryDate(int thresholdDays);
}
