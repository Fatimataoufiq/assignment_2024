package ma.nice.store.service;

import ma.nice.store.dto.request.ReturnRequest;
import ma.nice.store.exceptions.ProductNotFound;

public interface ReturnProductService {
     Long returnProduct(ReturnRequest returnRequest) throws ProductNotFound ;

}
