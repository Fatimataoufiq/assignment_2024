package ma.nice.store.service;

import ma.nice.store.entity.Product;
import ma.nice.store.dto.request.ProductRequest;
import ma.nice.store.dto.request.SetThresholdRequest;
import ma.nice.store.exceptions.ProductAlreadyExists;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.exceptions.ProductValidationException;

import java.util.List;

public interface ProductService {
    Long addProduct(ProductRequest productCommand) throws ProductAlreadyExists, ProductValidationException;

    List<ProductRequest> listProducts();

    ProductRequest getProductById(Long id) throws ProductNotFound;
    Product getProductByCode(String productCode) throws ProductNotFound;

    boolean deleteProduct(Long id) throws ProductNotFound;

    void setThreshold(SetThresholdRequest thresholdRequest) throws ProductNotFound;

    List<ProductRequest> getAllProductsBelowThreshold();
}
