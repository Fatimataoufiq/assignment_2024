package ma.nice.store.service;

import ma.nice.store.entity.Product;
import ma.nice.store.entity.util.EventType;

public interface TransactionHistoryService {
    Long createTransaction(Product product, EventType eventType, Integer quantity);
}
