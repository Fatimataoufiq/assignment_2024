package ma.nice.store.service;

import ma.nice.store.dto.request.SaleRequest;
import ma.nice.store.exceptions.ProductNotFound;
import ma.nice.store.exceptions.ProductQuantityNotInStock;

public interface SaleService {
    Long addSale(SaleRequest saleRequest) throws ProductNotFound, ProductQuantityNotInStock;
}
