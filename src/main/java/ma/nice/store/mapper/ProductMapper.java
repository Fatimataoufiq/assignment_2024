package ma.nice.store.mapper;

import ma.nice.store.entity.Product;
import ma.nice.store.dto.request.ProductRequest;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    private final ModelMapper modelMapper;


    public ProductMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public ProductRequest toDTO(Product product) {
        return modelMapper.map(product, ProductRequest.class);
    }

    public Product toEntity(ProductRequest productRequest) {
        return modelMapper.map(productRequest, Product.class);
    }

}
