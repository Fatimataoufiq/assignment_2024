package ma.nice.store.mapper;

import ma.nice.store.entity.Sale;
import ma.nice.store.dto.request.SaleRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SaleMapper {
    private final ModelMapper modelMapper;

    @Autowired
    public SaleMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public SaleRequest toDTO(Sale sale) {
        return modelMapper.map(sale, SaleRequest.class);
    }

    public Sale toEntity(SaleRequest saleRequest) {
        return modelMapper.map(saleRequest, Sale.class);
    }

}
