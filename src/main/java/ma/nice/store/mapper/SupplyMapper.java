package ma.nice.store.mapper;

import ma.nice.store.entity.Supply;
import ma.nice.store.dto.request.SupplyRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SupplyMapper {
    private final ModelMapper modelMapper;

    @Autowired
    public SupplyMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public SupplyRequest toDTO(Supply supply) {
        return modelMapper.map(supply, SupplyRequest.class);
    }

    public Supply toEntity(SupplyRequest supplyRequest) {
        return modelMapper.map(supplyRequest, Supply.class);
    }

}
