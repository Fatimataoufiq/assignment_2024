package ma.nice.store.mapper;

import ma.nice.store.entity.TransactionHistory;
import ma.nice.store.dto.request.TransactionHistoryRequest;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {
    private static final ModelMapper modelMapper = new ModelMapper();

    public static TransactionHistoryRequest toDTO(TransactionHistory transactionHistory){
        return modelMapper.map(transactionHistory, TransactionHistoryRequest.class);
    }

    public static TransactionHistory toEntity(TransactionHistoryRequest transactionHistoryRequest){
        return modelMapper.map(transactionHistoryRequest, TransactionHistory.class);
    }

}
