package ma.nice.store.mapper;

import ma.nice.store.entity.ReturnProduct;
import ma.nice.store.dto.request.ReturnRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReturnProductMapper {
    private final ModelMapper modelMapper;

    @Autowired
    public ReturnProductMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public ReturnRequest toDTO(ReturnProduct returnProduct) {
        return modelMapper.map(returnProduct, ReturnRequest.class);
    }

    public ReturnProduct toEntity(ReturnRequest returnRequest) {
        return modelMapper.map(returnRequest, ReturnProduct.class);
    }
}
