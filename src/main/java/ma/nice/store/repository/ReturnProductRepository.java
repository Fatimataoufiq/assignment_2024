package ma.nice.store.repository;

import ma.nice.store.entity.ReturnProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ReturnProductRepository extends JpaRepository<ReturnProduct, Long> {
}
